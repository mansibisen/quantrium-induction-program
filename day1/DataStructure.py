# dict

mydict = dict()
mydict['apple']= 1
mydict['banana']= 2
mydict['orange'] = 3
print(mydict)
for key , value in mydict.items():
    print(f'fruit name :{key} , id : {value}')

mydict['apple']= mydict['orange']+mydict['banana']
print(mydict)
#error
#print(mydict['grapes'])

# list
mylist = list()
for i in range(0 , 10):
    mylist.append(i)
print(mylist)
print(len(mylist))
print(mylist[1])
mylist[1]= 100
print(mylist)
mylist.pop(1)
print(mylist)

#tuple

mytuple = ("USA" , "INDIA" , "FRANCE")
print(mytuple)
print(mytuple[0])
#error
#mytuple[0] = 'NZ'

myset = set()
for i in range(0 , 10):
    myset.add(i)
print(myset)
myset.add(8)
myset.add(8)

#error
#print(myset[0])
