import os
import shutil

#Identify the file type and segregate the files in a folder and move them to separate respective folders. Create folders if not present. 
dir = os.getcwd()
print(f' current director is :{dir}')
path = os.path.join(dir, 'files')
os.mkdir(path)
list = ['one' , 'two' ,'three' , 'four' , 'five']


for i in range(0 , 5):
    f = open(os.path.join(path , list[i]+'.txt') , 'w')
    f.close()
for i in range(0 , 5):
    f = open(os.path.join(path , list[i]+'.pdf')  , 'w')
    f.close()
dir_list = os.listdir(path) 

pdfpath =os.path.join(dir, 'pdf')
txtpath = os.path.join(dir, 'txt')
os.mkdir(pdfpath)
os.mkdir(txtpath)

for file in dir_list:
    filepath = os.path.join(path, file)
    name, extension = os.path.splitext(filepath)
    if extension == '.pdf':
        os.rename(filepath, os.path.join(pdfpath, file))
    else:
        os.rename(filepath, os.path.join(txtpath, file))

    



#Reading the file properties - file size, last modified, file type, permissions, etc.
myfilepath = os.path.join(pdfpath, 'one.pdf')
size = os.path.getsize(myfilepath) 
print('Size :', size)

time = os.path.getmtime(myfilepath)

print('time:' , time)

name, extension = os.path.splitext(myfilepath)
print('file type:' , extension)

status = os.stat(myfilepath)
print("file permission :", status.st_mode)

