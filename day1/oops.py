import math

class shape:
    def __init__(self,  x=0  , y =0):
         self.dim1 = x
         self.dim2 = y
    def get_dimension(self):
        return self.dim1 , self.dim2
      
    # setter method
    def set_dimension(self, x , y ):
        self.dim1 = x
        self.dim2 = y
    

class Rectangle(shape):
    
        
    def __init__(self, x = 0, y =0):
        super().__init__(x, y)
        
    
    
    def area(self):
        return self.dim1 * self.dim2
    
class Square(shape):
    def __init__(self, x = 0, y =0):
        super().__init__(x, y)
    def get_dimension(self):
        return self.dim1 
      
    # setter method
    def set_dimension(self, x ):
        self.dim1 = x
        
    def area(self):
        return self.dim1 * self.dim1
    


# for rectangle
Rect = Rectangle()
Rect.set_dimension(10 , 20)
x , y = Rect.get_dimension()
a = Rect.area()
print(f'Rect length: {x} , Rect width:{ y}  , area :{a}')

# for square
Sqr = Square()
Sqr.set_dimension(10)
x = Sqr.get_dimension()
a = Sqr.area()

print(f'Sqr length: {x}, area :{a} ')


#Adding traingle and circle class
class Triangle(shape):
    def __init__(self, x = 0, y =0):
        super().__init__(x, y)
    
    def area(self):
        return self.dim1 * self.dim2/2

class Circle(shape):
    def __init__(self, x = 0, y =0):
        super().__init__(x, y)
    def get_dimension(self):
        return self.dim1 
      
    # setter method
    def set_dimension(self, x ):
        self.dim1 = x
    def area(self):
        return  self.dim1*self.dim2*math.pi
    
# for triangle
Tri = Triangle()
Tri.set_dimension(10 , 5)
x , y = Tri.get_dimension()
a = Tri.area()
print(f'Tri base: {x} , Tri height:{ y}, area :{a} ')

# for circle
Cri = Circle()
Cri.set_dimension(10 )
x  = Cri.get_dimension()
a = Tri.area()
print(f'Cri radius: {x} ,  area :{a} ')
